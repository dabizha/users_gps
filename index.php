<?php

const BASE_DIR = __DIR__;

require_once BASE_DIR . '/vendor/autoload.php';

(new App\App())->response();
