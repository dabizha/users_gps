<?php


namespace App\Models;

/**
 * Class Country
 * @package App\Models
 */
class Country extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'countries';

}
