<?php


namespace App\Models;

class User extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function get($query, $params)
    {
        if (!$result = $this->db->select($query, $params)) {
            return null;
        }

        $response = null;

        foreach ($result as $item) {
            $response = $item;
            break;
        }

        return $response;
    }

    /**
     * @param $query
     * @param $params
     * @return bool|int
     */
    public function update($query, $params)
    {
        return $this->db->update($query, $params);
    }

    /**
     * @param $query
     * @param $params
     * @return bool|int
     */
    public function insert($query, $params)
    {
        return $this->db->insert($query, $params, $this->table);
    }

    /**
     * @param $query
     * @param $params
     * @return bool
     */
    public function delete($query, $params)
    {
        return $this->db->delete($query, $params);
    }
}
