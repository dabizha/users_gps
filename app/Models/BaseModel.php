<?php


namespace App\Models;

use App\Common\DB;
use App\Interfaces\IModel;

/**
 * Class BaseModel
 * @package App\Models
 */
abstract class BaseModel implements IModel
{
    /**
     * @var DB
     */
    protected $db;

    /**
     * @var
     */
    protected $table;

    /**
     * BaseModel constructor.
     */
    public function __construct()
    {
        $this->db = new DB();
    }

    /**
     * @param $query
     * @param $params
     * @return array|bool
     */
    public function select($query, $params)
    {
        return $this->db->select($query, $params);
    }

    /**
     * @return array|bool
     */
    public function getAll()
    {
        return $this->db->select('SELECT * FROM ' . $this->table, []);
    }

}
