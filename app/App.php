<?php


namespace App;


use App\Controllers\HomeController;
use App\Controllers\UsersController;

/**
 * Class App
 * @package App
 */
class App
{
    public function response()
    {
        $rUri = $_SERVER['REQUEST_URI'];

        $rUriExploded = explode('?', $rUri)[0];

        try {
            switch ($rUriExploded) {
                case '/users/add':
                    (new UsersController())->add();
                    break;
                case '/users/edit':
                    (new UsersController())->edit();
                    break;
                case '/users/update':
                    (new UsersController())->update();
                    break;
                case '/users/delete':
                    (new UsersController())->delete();
                    break;
                case '/users/check_email':
                    (new UsersController())->check();
                    break;
                case '/users/save':
                    (new UsersController())->save();
                    break;
                case '/':
                default:
                    (new HomeController())->index();
                    break;
            }
        } catch (\Exception $e) {
            // To log

            (new HomeController())->index();
        }
    }

}
