<?php


namespace App\Controllers;


use App\Models\User;
use App\Views\View;

/**
 * Class BaseController
 * @package App\Controllers
 */
class BaseController
{
    /**
     *
     */
    public function homePage()
    {
        $model = new User();

        $query = "SELECT `users`.*, `countries`.`name` as `country_name` FROM `users` 
                JOIN `countries` ON `users`.`country_id`=`countries`.`id`";

        $allUsers = $model->select($query, []);

        View::view('home/index', ['users' => $allUsers]);
    }

}
