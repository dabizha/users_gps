<?php


namespace App\Controllers;


use App\Models\Country;
use App\Models\User;
use App\Views\View;

/**
 * Class UsersController
 * @package App\Controllers
 */
class UsersController extends BaseController
{
    /**
     *
     */
    public function add()
    {
        $countriesModel = new Country();

        $countries = $countriesModel->getAll();

        View::view('users/add', ['countries' => $countries]);
    }

    /**
     * @throws \Exception
     */
    public function edit()
    {
        $userId = (int)$_GET['id'] ?? null;

        if (!$userId) {
            throw new \Exception();
        }

        $userModel = new User();

        $query = "SELECT `users`.*, `countries`.`name` as `country_name` FROM `users` 
                JOIN `countries` ON `users`.`country_id`=`countries`.`id`
                WHERE `users`.`id`=:id";

        $user = $userModel->get($query, ['id' => $userId]);

        $countriesModel = new Country();

        $countries = $countriesModel->getAll();

        View::view('users/edit', ['user' => $user, 'countries' => $countries]);
    }

    /**
     * @throws \Exception
     */
    public function update()
    {
        $params = [];

        $whiteLabel = ['user_id', 'user_name', 'user_email', 'user_country', 'user_new_country'];

        foreach ($_POST as $key => $item) {
            if (!in_array($key, $whiteLabel)) {
                continue;
            }

            $params[$key] = strip_tags($item);
        }

        if (count($whiteLabel) != count($params)) {
            throw new \Exception();
        }

        if ((int)$params['user_new_country'] && $params['user_country'] != $params['user_new_country']) {
            $params['user_country'] = $params['user_new_country'];
        }

        unset($params['user_new_country']);

        $model = new User();

        $query = "UPDATE `users` 
        SET `users`.`name`=:user_name, `users`.`email`=:user_email, `users`.`country_id`=:user_country
        WHERE `users`.`id`=:user_id
        ";

        try {
            $model->update($query, $params);
        } catch (\Throwable $t) {
            throw new \Exception();
        }

        $this->homePage();
    }

    /**
     *
     */
    public function check()
    {
        header('Content-type: application/json');

        $email = $_GET['email'] ?? null;

        if (!$email) {
            echo json_encode(['code' => '208']);
            exit;
        }

        $model = new User();

        $allUsers = $model->getAll();

        foreach ($allUsers as $user) {
            if ($email == $user['email']) {
                echo json_encode(['code' => '208']);
                exit;
            }
        }

        echo json_encode(['code' => '200']);
        exit;
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        $params = [];

        $whiteLabel = ['user_name', 'user_email', 'country_id'];

        foreach ($_POST as $key => $item) {
            if (!in_array($key, $whiteLabel)) {
                continue;
            }

            $params[$key] = strip_tags($item);
        }

        if (count($whiteLabel) != count($params)) {
            throw new \Exception();
        }

        if (!$params['country_id']) {
            $params['country_id'] = 1;
        }

        $model = new User();

        $query = 'INSERT INTO users (`users`.`name`, `users`.`email`, `users`.`country_id`) VALUES (:user_name, :user_email, :country_id)';

        if (!$model->insert($query, $params)) {
            throw new \Exception();
        }

        $this->homePage();
    }

    /**
     * @throws \Exception
     */
    public function delete()
    {
        $id = (int)$_GET['id'] ?? null;

        if (!$id) {
            throw new \Exception();
        }

        $model = new User();

        if (!$model->delete('DELETE FROM `users` WHERE `users`.`id`=:id', ['id' => $id])) {
            throw new \Exception();
        }

        $this->homePage();
    }

}
