<?php

namespace App\Views;

/**
 * Class View
 * @package App\Views
 */
class View
{
    /**
     * @param $pathToHtml
     * @param $data
     */
    public static function view($pathToHtml, $data)
    {
        extract($data);

        $view = BASE_DIR . '/assets/views/' . $pathToHtml . '.php';

        require_once BASE_DIR . '/assets/template.php';
    }

}
