<?php


namespace App\Interfaces;


/**
 * Interface IModel
 * @package App\Interfaces
 */
interface IModel
{
    /**
     * @param $query
     * @param $params
     * @return mixed
     */
    public function select($query, $params);

    /**
     * @return mixed
     */
    public function getAll();

}
