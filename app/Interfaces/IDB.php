<?php


namespace App\Interfaces;

/**
 * Interface IDB
 */
interface IDB
{
    /**
     * @param string $query
     * @param array $params
     * @return mixed
     */
    public function select(string $query, array $params);

    /**
     * @param string $query
     * @param array $params
     * @return bool
     */
    public function update(string $query, array $params);

    /**
     * @param string $query
     * @param array $params
     * @param string $tableName
     * @return mixed
     */
    public function insert(string $query, array $params, string $tableName);

    /**
     * @param string $query
     * @param array $params
     * @return bool
     */
    public function delete(string $query, array $params);

}
