<?php

namespace App\Common;


use App\Interfaces\IDB;

/**
 * Class DB
 * @package App\Common
 */
class DB implements IDB
{
    /**
     * @var \PDO|null
     */
    private $db;

    /**
     * DB constructor.
     */
    public function __construct()
    {
        $this->db = DBConnection::instance();
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool|mixed|\PDOStatement
     */
    public function select(string $query, array $params)
    {
        $data = $this->db->prepare($query);

        $data->execute($params);

        return $data;
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool|int
     */
    public function update(string $query, array $params)
    {
        $data = $this->db->prepare($query);

        return $data->execute($params);
    }

    /**
     * @param string $query
     * @param array $params
     * @param string $tableName
     * @return bool|int
     */
    public function insert(string $query, array $params, string $tableName)
    {
        $data = $this->db->prepare($query);

        $data->execute($params);

        $result = $this->select('SELECT * FROM ' . $tableName . ' ORDER BY id DESC limit 1', []);

        if (!$result) {
            return false;
        }

        $lastId = 0;

        foreach ($result as $item) {
            $lastId = $item['id'];
        }

        return $lastId;
    }

    /**
     * @param string $query
     * @param array $params
     * @return bool
     */
    public function delete(string $query, array $params)
    {
        $data = $this->db->prepare($query);

        return $data->execute($params);
    }

}
