<?php


namespace App\Common;


/**
 * Class DBConnection
 * @package App\Common
 */
class DBConnection
{
    /**
     * @var null
     */
    private static $instance = null;

    /**
     * DBConnection constructor.
     */
    private function __construct()
    {
    }

    /**
     *
     */
    private function __clone()
    {
    }

    /**
     *
     */
    private function __wakeup()
    {
    }

    /**
     * @return \PDO|null
     */
    public static function instance()
    {
        if (null != self::$instance) {
            return self::$instance;
        }

        self::$instance = self::getConnection();

        return self::$instance;
    }

    /**
     * @return \PDO
     */
    private static function getConnection()
    {
        $configsArray = config('db', 'mysql');

        $dsn = 'mysql:host=' . $configsArray['db_host'] . ';dbname=' . $configsArray['db_name'] . ';charset=' . $configsArray['db_charset'];

        $opt = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        return new \PDO($dsn, $configsArray['db_user'], $configsArray['db_password'], $opt);
    }

}

