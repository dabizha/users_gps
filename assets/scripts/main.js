$(document).ready(function () {
    $('#inputEmail').on("focusout", function() {
        let email = this.value;

        $.ajax({
            url: "/users/check_email",
            method: 'get',
            data: {
                email: email
            },
            success: function(data){
                console.log(data.code);

                if (data.code != 200) {
                    $('#inputEmail').val('');
                }
            },
            error: function(data) {
                $('#inputEmail').value = '';
            }
        });
    });
});
