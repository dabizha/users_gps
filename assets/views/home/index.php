<table class="table">
  <thead class="thead-dark">
  <tr>
    <th scope="col">#</th>
    <th scope="col">Name</th>
    <th scope="col">Email</th>
    <th scope="col">Country</th>
    <th scope="col">Action</th>
  </tr>
  </thead>
  <tbody>
  <?php foreach($users as $user): ?>
    <tr>
      <td scope="row"><?= $user['id'] ?></td>
      <td><?= $user['name'] ?></td>
      <td><?= $user['email'] ?></td>
      <td><?= $user['country_name'] ?></td>
      <td>
        <a href="/users/edit?id=<?= $user['id'] ?>"><i class="fas fa-user-edit"></i></a>
        <a href="/users/delete?id=<?= $user['id'] ?>"><i class="fas fa-user-times delete-user"></i></a>
      </td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>
