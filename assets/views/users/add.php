<?php //var_dump($countries);
//die(); ?>

<form class="form" method="POST" action="/users/save">
  <div class="form-group">
    <label for="inputName">Name</label>
    <input type="text" class="form-control" name="user_name" id="inputName" required>
  </div>
  <div class="form-group">
    <label for="inputEmail">Email</label>
    <input type="email" class="form-control" name="user_email" id="inputEmail" required>
  </div>
  <div class="form-group">
    <label for="select_country">Country</label>
    <select class="form-control" id="select_country" name="country_id">
      <option>...</option>
        <?php foreach ($countries as $country): ?>
          <option value="<?= $country['id'] ?> "><?= $country['name'] ?></option>
        <?php endforeach; ?>
    </select>
  </div>

  <button type="submit" class="btn btn-primary" id="submit">Submit</button>
</form>

