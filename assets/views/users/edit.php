<?php //var_dump($user);
//die(); ?>

<form class="form" method="POST" action="/users/update">
  <div class="form-group">
    <input type="hidden" name="user_id" value="<?= $user['id'] ?>">
  </div>
  <div class="form-group">
    <label for="inputName">Name</label>
    <input type="text" class="form-control" name="user_name" id="inputName" value="<?= $user['name'] ?>" required>
  </div>
  <div class="form-group">
    <label for="inputEmail">Email</label>
    <input type="email" class="form-control" name="user_email" id="inputEmail" value="<?= $user['email'] ?>" required>
  </div>
  <div class="form-group">
    <input type="hidden" name="user_country" value="<?= $user['country_id'] ?>">
    <label for="inputCountry">User Country</label>
    <input type="text" class="form-control" id="inputCountry" value="<?= $user['country_name'] ?>" required>
  </div>
  <div class="form-group">
    <label for="select_country">Set New Country</label>
    <select class="form-control" id="select_country" name="user_new_country">
      <option value="0">...</option>
        <?php foreach ($countries as $country): ?>
          <option
              value="<?= $country['id'] ?> <?php if ($country['id'] == $user['country_id']): ?> selected <?php endif; ?>"><?= $country['name'] ?></option>
        <?php endforeach; ?>
    </select>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>


