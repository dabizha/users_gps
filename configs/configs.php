<?php

function config($type, $key)
{
    $configs = [
        'db' => [
            'mysql' => [
                'db_host' => '',
                'db_name' => '',
                'db_user' => '',
                'db_password' => '',
                'db_charset' => '',
            ]
        ]
    ];

    return $configs[$type][$key]?? null;
}
